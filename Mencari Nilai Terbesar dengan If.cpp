#include<iostream>
#include<conio.h>
using namespace std;
int main()
{
	cout << "Program Mencari Nilai Terbesar dengan IF \n";
	cout << "---------------------------------------- \n";
	int A, B, C;
	
	cout << "Masukkan Nilai A: ";
	cin >> A;
	cout << "Masukkan Nilai B: ";
	cin >> B;
	cout << "Masukkan Nilai C: ";
	cin >> C;
	cout << "\n";
	
	if (A > B)
	{
		if (A > C)
		{
			cout << "Nilai Terbesar adalah " << A;
		} else if (A < C)
		{
			cout << "Nilai Terbesar adalah " << C;
		}
	} else if (A < B)
	{
		if (B > C)
		{
			cout << "Nilai Terbesar adalah " << B;
		}else if (B < C)
		{
			cout << "Nilai Terbesar adalah " << C;
		}
	}
	
	getch();
}
